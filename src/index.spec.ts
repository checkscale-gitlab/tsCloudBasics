import * as si from 'systeminformation';
import { ISystemInformation, getResult, format } from './index';

function isISI(object: any): object is ISystemInformation {
  return 'cpu' in object;
}

describe('typeScript test suite', () => {
  it('should return an ISI type', async () => {
    expect.assertions(2);
    const res = await getResult([si.diskLayout]);
    const iSystemInformation = format(res);

    expect(isISI(iSystemInformation)).toBe(true);
    expect(isISI({})).toBe(false);
  });
});
