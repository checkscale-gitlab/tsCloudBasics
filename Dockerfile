 # stage compilation
 FROM alpine:3.15 as builder

# chemin de travail
 WORKDIR /app

 # downgrade des privilèges
 USER root

 # installation des paquets système
 RUN apk update && apk upgrade
 RUN apk add git
 RUN apk add nodejs>16 && apk add npm>8
 RUN apk update && apk upgrade

 # installation des dépendances avec npm
 RUN git clone https://gitlab.com/EnzoEnzoo/tsCloudBasics.git
 WORKDIR /app/tsCloudBasics
 RUN npm install --only=production
 RUN cp -r node_modules prod_modules
 RUN npm install

 # build avec npm
 RUN npm run build



 # stage exécution
 FROM alpine:3.15 as runner

# chemin de travail
 WORKDIR /app

 # downgrade des privilèges
 USER root

 # installation des paquets système
 RUN apk update && apk upgrade
 RUN apk add nodejs>16
 RUN apk update && apk upgrade

# copie des répertoires nécessaires à l'exécution
 COPY --from=builder --chown=root:root /app/tsCloudBasics/dist/ dist
 COPY --from=builder --chown=root:root /app/tsCloudBasics/prod_modules/ node_modules

# diminution droits user
# USER user

# exécution
 CMD ["node", "dist/index.js"]
